﻿using System;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class CanvasHide : MonoBehaviourPunCallbacks, IPunInstantiateMagicCallback
{
    public static GameObject LocalPlayerInstance;
    
    private void Awake()
    {
        
    }

    void Start()
    {

    }

    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        Debug.Log(info.photonView.Owner.ToString());
        Debug.Log(info.photonView.ViewID.ToString());
        
        if (photonView.IsMine)
        {
            LocalPlayerInstance = gameObject;
        }
        else
        {
            GetComponentInChildren<Canvas>().enabled = false;
        }
    }

}
