﻿using System;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UIElements;
using Slider = UnityEngine.UI.Slider;

public class MoveCharacter2D : MonoBehaviourPun
{
    private float _horizontalMove = 0;
    private float _verticalMove = 0;

    public int moveSpeed;
    public bool Left = false;
    public bool Right = false;
    public bool Up = false;
    public bool Down = false;
    

    public GameObject Atk;
    public Camera m_playerCam;
    public Transform PlayerPos;

    public PunHealth m_Hp;
    public Animator animator;
    

    void Awake()
    {
        
    }

    void Start()
    {
        m_playerCam = this.GetComponentInChildren<Camera>();
        m_Hp = GetComponent<PunHealth>();
    }

    // Update is called once per frame
    void Update()
    {
        if (m_Hp.zeroHP == true)
        {
            return;
        }
        
        GetPlayerInput();
        MovePlayer();
        
        if (Input.GetKeyDown(KeyCode.Space))
        {
            CmdFire();
        }
        else
        {
            animator.SetBool("ATK",false);
        }

        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
        {
            Left = true;
            Right = false;
            Up = false;
            Down = false;
        }
        
        else if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            Left = false;
            Right = true;
            Up = false;
            Down = false;
        }
        
        else if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            Up = true;
            Down = false;
            Left = false;
            Right = false;
        }
        
        else if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
        {
            Up = false;
            Down = true;
            Left = false;
            Right = false;
        }
        /*else
        {
            Atk.SetActive(false);
            CmdFire();
        }*/
    }

    private void FixedUpdate()
    {
        
    }

    private void GetPlayerInput()
    {
        _horizontalMove = Input.GetAxisRaw("Horizontal");
        _verticalMove = Input.GetAxisRaw("Vertical");
    }

    private void MovePlayer()
    {
        Vector3 directionVector = new Vector3(_horizontalMove,_verticalMove,0);
        transform.Translate(directionVector.normalized*Time.deltaTime*moveSpeed);
    }

    void CmdFire()
    {
        object[] data = {photonView.ViewID};
        
        if (Right == true && Left == false)
        {
            GameObject Attack = PhotonNetwork.Instantiate(this.Atk.name, PlayerPos.transform.position + (PlayerPos.transform.right * 5f), Quaternion.identity, 0);

            Destroy(Attack.gameObject, 0.1f);
        }

        else if (Left == true && Right == false)
        {
            GameObject Attack = PhotonNetwork.Instantiate(this.Atk.name, PlayerPos.transform.position + (PlayerPos.transform.right * -5f), Quaternion.identity, 0);

            Destroy(Attack.gameObject, 0.1f);
        }
        
        if (Up == true && Down == false)
        {
            GameObject Attack = PhotonNetwork.Instantiate(this.Atk.name, PlayerPos.transform.position + (PlayerPos.transform.up * 5f), Quaternion.identity, 0);

            Destroy(Attack.gameObject, 0.1f);
        }
        
        else if (Up == false && Down == true)
        {
            GameObject Attack = PhotonNetwork.Instantiate(this.Atk.name, PlayerPos.transform.position + (PlayerPos.transform.up * -6f), Quaternion.identity, 0);

            Destroy(Attack.gameObject, 0.1f);
        }
        
        animator.SetBool("ATK",true);

    }
}
