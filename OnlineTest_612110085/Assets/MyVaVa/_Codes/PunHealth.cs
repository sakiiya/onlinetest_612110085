﻿using System;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class PunHealth : MonoBehaviourPunCallbacks, IPunObservable
{
    public Slider slider;
    public float Health = 30;
    public bool Death = false;
    public Animator animator;
    public bool zeroHP = false;

    void Update()
    {
            Health -= Time.deltaTime;
            slider.value = Health;

            if (Health >= 30)
            {
                Health = 30;
            }

            if (Health <= 0)
            {
                Health = 0;
                if (Health == 0)
                {
                    Death = true;
                    zeroHP = true;
                    animator.SetBool("Die",true);
                }
            }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.IsWriting)
        {
            stream.SendNext(Health);
        }
        else
        {
            Health = (float) stream.ReceiveNext();
        }
    }

    public void Healing(int amount)
    {
        Health += amount;
    }
    
    public void Tranfrom_Healing(int amount, int OwnerNetID)
    {
        Health += amount;
        
    }

    public void TakeDamage(int amount, int OwnerNetID)
    {
        if (photonView != null)
            photonView.RPC("PunRPCTakedDamage", RpcTarget.All, amount, OwnerNetID);
        else print("photonView is NULL.");
        
        
    }

    [PunRPC]
    public void PunRPCTakedDamage(int amount, int OwnerNetID)
    {
        Health -= amount;
        Debug.Log("Take Damage" + Health);
        if (Health <= 0)
        {
            Health = 0;
            
            Debug.Log("BulletID : " + OwnerNetID.ToString() + " Killed " + photonView.ViewID);
            if (Health == 0)
            {
                Death = true;
                zeroHP = true;
            }

            /*if (Death == true)
            {
                Death = false;
                
            }*/
            animator.SetBool("Die",true);
        }
    }

    /*public virtual void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        
        if (stream.isWriting)
        {
            stream.SendNext(Health);
        } else if (stream.isReading)
        {
            Health = (float)stream.ReceiveNext();
        }
    }*/
    /*[PunRPC]
    void Damage(GameObject WhichPlayer)
    {
        Health -= 10;
        slider.value = Health;
    }*/
}
