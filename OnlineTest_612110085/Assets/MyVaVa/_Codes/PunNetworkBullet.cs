﻿using System;
using UnityEngine;
using Photon.Pun;

[RequireComponent(typeof(PhotonTransformView))]
public class PunNetworkBullet : MonoBehaviourPun, IPunInstantiateMagicCallback
{
    [Range(1, 10)] 
    public int m_AmountDamage = 2;
    private int OwnerViewID = -1;
    
    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        info.Sender.TagObject = this.gameObject;
        OwnerViewID = info.photonView.ViewID;

        if (!photonView.IsMine)
        {
            return;
        }
        
        Destroy(this.gameObject, 0.1f);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!photonView.IsMine)
        {
          return;   
        }

        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("Bullet Collision to other player.");

            PunUserNetControl RBOther = other.gameObject.GetComponentInChildren<PunUserNetControl>();

            if (RBOther != null) 
            {
                Debug.Log("Attack to Other ViewID : " + RBOther.photonView.ViewID);
            }

            PunHealth RBHealOther = other.gameObject.GetComponentInChildren<PunHealth>();

            if (RBHealOther != null)
            {
                RBHealOther.TakeDamage(m_AmountDamage,photonView.ViewID);
            }
        }
        Destroy(this.gameObject);
    }

    private void OnDestroy()
    {
        if (!photonView.IsMine)
        {
            return;
        }
        PhotonNetwork.Destroy(this.gameObject);
    }
    
}
