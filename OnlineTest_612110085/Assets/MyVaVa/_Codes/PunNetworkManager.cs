﻿using UnityEngine;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class PunNetworkManager : ConnectAndJoinRandom
{
    public static PunNetworkManager singleton;
    
    [Header("Spawn Info")]
    [Tooltip("The prefab to use for representing the player")]
    public GameObject GamePlayerPrefab;
    
    public bool isGameStart = false;
    public bool isFirstSetting = false;
    public GameObject HealingPrefab;
    public int numberOfHealing = 2;
    private float m_count = 0;
    public float m_CountDownDropHeal = 10;
    public bool isGameOver = false;
    public delegate void PlayerSpawned();
    public static event PlayerSpawned OnPlayerSpawned;

    private void Awake() {
        singleton = this;
    }
    
    
    // Update is called once per frame
    void Update()
    {
        if (PhotonNetwork.IsMasterClient != true)
        {
            return;
        }

        if (isGameStart == true)
        {
            AirDropHealing();
        }
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        Camera.main.gameObject.SetActive(false);

        if (PunUserNetControl.LocalPlayerInstance == null)
        {
            Debug.Log("I'm in :" + SceneManagerHelper.ActiveSceneName);
            PunNetworkManager.singleton.SpawnPlayer();
        }
        else
        {
            Debug.Log("I'm OUT Bitch " + SceneManagerHelper.ActiveSceneName);
        }
    }

    public void SpawnPlayer()
    {
        PhotonNetwork.Instantiate(GamePlayerPrefab.name, new Vector2(Random.Range(-57.5f, 62.5f), Random.Range(29.1f, -41.8f)), Quaternion.identity, 0);
        isGameStart = true;
    }

    void AirDropHealing()
    {
        if (isFirstSetting == false)
        {
            isFirstSetting = true;
            int half = numberOfHealing / 2;
            for (int i = 0; i < half; i++)
            {
                PhotonNetwork.InstantiateSceneObject(HealingPrefab.name,
                    AirDrop.RandomPosition(5f),
                    Quaternion.identity, 0);
            }

            m_count = m_CountDownDropHeal;
        }
        else
        {
            if (GameObject.FindGameObjectsWithTag("Healing").Length < numberOfHealing)
            {
                m_count -= Time.deltaTime;

                if (m_count <= 0)
                {
                    m_count = m_CountDownDropHeal;
                    PhotonNetwork.Instantiate(HealingPrefab.name,
                        AirDrop.RandomPosition(10f),
                        Quaternion.identity, 0);
                }
            }
        }
    }
}
