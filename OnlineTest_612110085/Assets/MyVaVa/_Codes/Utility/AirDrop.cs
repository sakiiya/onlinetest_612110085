﻿using UnityEngine;

public class AirDrop
{
	public static Vector3 RandomPosition(float yOffset)
	{
		var spawnPosition = new Vector2(
			Random.Range(-107.3f, 105.4f),
			Random.Range(83.6f, -81.4f));
		return spawnPosition;
	}
}
